package com.s4m.upgrader;


import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import org.apache.commons.io.input.CloseShieldInputStream;
import org.apache.log4j.Logger;

import com.s4m.upgrader.config.AppConfig;
import com.s4m.upgrader.config.ConfigLoader;
import com.s4m.upgrader.config.InstallationConfig;
import com.s4m.upgrader.installations.InstallationLoader;
import com.s4m.upgrader.installations.InstallationsLoaderImpl;
import com.s4m.upgrader.model.Installation;
import com.s4m.upgrader.upgrades.Upgrader;
import com.s4m.upgrader.utils.OSHelper;

public class App {
	
	private final InstallationLoader loader;
	public static final String DIR = System.getProperty("user.dir", "");
	public static String version;
	public static String instanceId;
	private static final Logger logger = Logger.getLogger(App.class);
	
	public static AppConfig appConfig;

	public AppConfig getAppConfig() {
		return appConfig;
	}

	public App() {
		this.getVersion();
		OSHelper.getInstance().isSupportedPlatform();
		loader = new InstallationsLoaderImpl();
		
	}
	
    public static void main(String[] args) {
    	System.out.println("# WELCOME IN UPGRADER, software for upgrading old platform Media Solutions products #\n");
    	String msgApp = "Application started in working directory " + DIR;
    	logger.info(msgApp);
    	System.out.println(msgApp);
    	new App().run();
    	msgApp = "Application has ended.";
    	logger.info("Application has ended");
    	System.out.println(msgApp);
    	
    	System.exit(0);
    }
    
    private void run() {
    	System.out.println("version: " + App.version + "\n");
    	appConfig = loadConfig();

	    Installation upgraderInstallation = loader.getInstallation(App.instanceId);
	    if(!checkVersion(upgraderInstallation)) {
	    	String msgVersionErr = "The current version (" + App.version + ") of the application does not match the version in CML (" + upgraderInstallation.getVersionNumber() + ")!!!";
	    	System.out.println(msgVersionErr + "\n");
	    	logger.error(msgVersionErr);
	    	
	    	System.exit(0);
	    }
	    
    	InstallationConfig installationConfig = selectInstallationConfig(appConfig);
	    	String msgSelectedInstallation = "Selected installation: " + installationConfig.getInstanceId();
	    	System.out.println(msgSelectedInstallation + "\n");
	    	logger.info(msgSelectedInstallation);
    	
    	String selectedBranch = selectBranch();
	    	String msgSelectedBranch = "Selected branch: " + selectedBranch;
	    	System.out.println(msgSelectedBranch + "\n");
	    	logger.info(msgSelectedBranch);
    	
    	Installation installation = loader.getInstallation(installationConfig.getInstanceId());
	    	String msgBuild = "Instance: " + installationConfig.getInstanceId() + ", current version: " + installation.getVersionNumber() + ", build date: " + installation.getBuildDate() + "\n";
	    	System.out.println(msgBuild);
	    	logger.info(msgBuild);
    		    
	    new Upgrader(installation, installationConfig, selectedBranch).run();
    	
	}

    private boolean checkVersion(Installation upgraderInstallation) {
    	return upgraderInstallation.getVersionNumber().equals(App.version);
    }
    
    private void getVersion() {
		final Properties properties = new Properties();
		try {
			properties.load(this.getClass(). getClassLoader().getResourceAsStream("project.properties"));
			App.version = properties.getProperty("version");
			App.instanceId = properties.getProperty("instanceId");
		} catch (IOException e) {
			String msgVersionError = "Unable to detect version of application!!!";
			logger.error(msgVersionError);
			throw new IllegalArgumentException(msgVersionError);
		}    	
    }
    
    private String selectBranch() {
    	
    	try (Scanner reader = new Scanner(new CloseShieldInputStream(System.in))) {
    		System.out.println("Enter upgrade branch \n[D]evel\n[P]roduction 2013");
    		String branch = reader.next();

			if (branch.toLowerCase().trim().equals("p")) { 
				return "2013"; 
			} else if (branch.toLowerCase().trim().equals("d")) {
				return "Devel";
			} 
							
			String msgSelectBranchError = "Branch '" + branch + "' does not exist!!";
			logger.error(msgSelectBranchError);
			throw new IllegalArgumentException(msgSelectBranchError);
						
    	}

    }

	private InstallationConfig selectInstallationConfig(AppConfig config) {
		System.out.println("Select installation:\n");
		
		List<InstallationConfig> installations = config.getInstallations();
		for (int i = 0; i < installations.size(); i++) {
			System.out.println("" + i + ". " + installations.get(i).getInstanceId());
		}
		
		try (Scanner reader = new Scanner(new CloseShieldInputStream(System.in))) {
			System.out.print("\nEnter installation index: ");
			int installationIndex = reader.nextInt();
			return installations.get(installationIndex);
		} catch (Exception e) {
			String msgWrongInstallationIndex = "You are stupid!\n";
			System.out.println(msgWrongInstallationIndex);
			logger.error(msgWrongInstallationIndex);
			throw new IllegalArgumentException(msgWrongInstallationIndex);
		}
	
	}
	
	private AppConfig loadConfig() {
		return new ConfigLoader().getConfig();
	}

}
