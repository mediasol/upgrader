package com.s4m.upgrader.config;

import java.util.ArrayList;

public class InstallationConfig {

	private String instanceId;
	private ArrayList<String> plugins = new ArrayList<String>();
	private String serviceName;
	private String mongoPrefix;

	public String getMongoPrefix() {
		return mongoPrefix;
	}

	public void setMongoPrefix(String mongoPrefix) {
		this.mongoPrefix = mongoPrefix;
	}

	public String getServiceName() {
		if (serviceName == null) return instanceId;
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public ArrayList<String> getPlugins() {
		return plugins;
	}

	public void setPlugins(ArrayList<String> plugins) {
		this.plugins = plugins;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	
}
