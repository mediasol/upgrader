package com.s4m.upgrader.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.s4m.upgrader.App;

public class ConfigLoader {
	
	private static final String FILE_NAME = "config.json";
	private final static Logger logger = Logger.getLogger(ConfigLoader.class);
	
	public AppConfig getConfig() {
		String workingDir = App.DIR;
		Path path = Paths.get(workingDir, FILE_NAME);
		if (!Files.exists(path)) {
			String msgConfigExistError = "Error while config file doesn't exist on path '" + path + "'!!";
			
			System.out.println(msgConfigExistError + "\n");
			logger.error(msgConfigExistError);
			throw new IllegalStateException(msgConfigExistError);
		}
			
		try {
			return new ObjectMapper().readValue(Files.readAllBytes(path), AppConfig.class);
		} catch (IOException e) {
			String msgConfigLoadError = "Error while loading app configuration!!";
			System.out.println(msgConfigLoadError + "\n");
			logger.error(msgConfigLoadError);
			throw new RuntimeException(msgConfigLoadError, e);
		}
	}
	
	
}
