package com.s4m.upgrader.config;

import java.util.List;

public class AppConfig {

	private List<InstallationConfig> installations;
	private String cmlUrl;
	private String builderUrl;
	private String backupPath;
	private String upgradePath;
	private String mongoServerBinPath;
	
	public String getMongoServerBinPath() {
		return mongoServerBinPath;
	}

	public void setMongoServerBinPath(String mongoServerBinPath) {
		this.mongoServerBinPath = mongoServerBinPath;
	}

	public String getUpgradePath() {
		return upgradePath;
	}

	public void setUpgradePath(String upgradePath) {
		this.upgradePath = upgradePath;
	}

	public String getBackupPath() {
		return backupPath;
	}

	public void setBackupPath(String backupPath) {
		this.backupPath = backupPath;
	}

	public String getBuilderUrl() {
		return builderUrl;
	}

	public void setBuilderUrl(String builderUrl) {
		this.builderUrl = builderUrl;
	}

	public String getCmlUrl() {
		return cmlUrl;
	}

	public void setCmlUrl(String cmlUrl) {
		this.cmlUrl = cmlUrl;
	}

	public List<InstallationConfig> getInstallations() {
		return installations;
	}

	public void setInstallations(List<InstallationConfig> installations) {
		this.installations = installations;
	}
	
}
