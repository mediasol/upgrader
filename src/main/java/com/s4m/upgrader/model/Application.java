package com.s4m.upgrader.model;

import java.util.Arrays;
import java.util.List;

public class Application extends NanoClass { 
	
	private String applicationId;

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
		
	}

	public String getShellCommand() {
		switch(this.applicationId) {
			case "CFM":
				return "cfmd";
			case "NX":
				return "naxosd";
			case "CFMBridge":
				return "cfmbridged";
			default:
				return null;
		}
	}
	
	public List<String> getMongoDbNames() {
		switch(this.applicationId) {
			case "CFM":
				return Arrays.asList( "history", "loginStats", "requests", "versions" );
			case "NX":
				return Arrays.asList( "detailVisits", "loginStats", "requests", "versions" );
			case "CFMBridge":
				return Arrays.asList( "loginStats" );
			default:
				return null;
		}
	}

}