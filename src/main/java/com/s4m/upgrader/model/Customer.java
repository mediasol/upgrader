package com.s4m.upgrader.model;

public class Customer extends NanoClass { 
	
	private String secondaryName;

	public String getSecondaryName() {
		return secondaryName;
	}

	public void setSecondaryName(String secondaryName) {
		this.secondaryName = secondaryName;
	}
	
	
}