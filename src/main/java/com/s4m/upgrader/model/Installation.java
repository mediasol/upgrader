package com.s4m.upgrader.model;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;

public class Installation {

	private String id;
	private boolean ignored;
	private String state;
	private String versionNumber;
	private String branch;
	private String buildDate;
	private DateTime lastHeartBeat;
	private long lastHeartBeatTimestamp;
	private Application application;
	private Customer customer;
	private String appLocation;
	private String wsLocation;
	private List<Path> untouchableFiles = new ArrayList<>();
	
	private void initUntouchableFiles() {
		addUntouchableFile("conf" + File.separator + "wrapper.conf");
		
		addUntouchableFile("cfmd");
		addUntouchableFile("cfm.rc");
		addUntouchableFile("cfmserver.ini");
		addUntouchableFile("cfmbridge.rc");
		addUntouchableFile("cfmbridged");
		addUntouchableFile("cfmbridge.ini");
		addUntouchableFile("naxos.rc");
		addUntouchableFile("naxosd");
		addUntouchableFile("naxos.ini");
		
	}
	
	private void addUntouchableFile(String path) {
		Path p = Paths.get(appLocation, path);
		if (Files.exists(p, LinkOption.NOFOLLOW_LINKS)) {
			untouchableFiles.add(p);
		}
	}
	
	public List<Path> getUntouchableFiles() {
		return untouchableFiles;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public boolean isIgnored() {
		return ignored;
	}

	public void setIgnored(boolean ignored) {
		this.ignored = ignored;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getBuildDate() {
		return buildDate;
	}

	public void setBuildDate(String buildDate) {
		this.buildDate = buildDate;
	}

	public DateTime getLastHeartBeat() {
		return lastHeartBeat;
	}

	public void setLastHeartBeat(DateTime lastHeartBeat) {
		this.lastHeartBeat = lastHeartBeat;
	}

	public long getLastHeartBeatTimestamp() {
		return lastHeartBeatTimestamp;
	}

	public void setLastHeartBeatTimestamp(long lastHeartBeatTimestamp) {
		this.lastHeartBeatTimestamp = lastHeartBeatTimestamp;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getAppLocation() {
		return appLocation;
	}

	public void setAppLocation(String appLocation) {
		this.appLocation = appLocation;
		this.initUntouchableFiles();
	}

	public String getWsLocation() {
		return wsLocation;
	}

	public void setWsLocation(String wsLocation) {
		this.wsLocation = wsLocation;
	}

}