package com.s4m.upgrader.utils;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.log4j.Logger;

public class Dumper {
	
	final static Logger logger = Logger.getLogger(Dumper.class);
	
	public void dump(Object obj) {
		logger.info(ToStringBuilder.reflectionToString(obj, ToStringStyle.SHORT_PREFIX_STYLE));	
	}
	
}