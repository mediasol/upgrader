package com.s4m.upgrader.utils;

import java.io.File;
//import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Path;
//import java.io.InputStreamReader;
import java.nio.file.Paths;

import org.apache.log4j.Logger;

import com.s4m.upgrader.App;
import com.s4m.upgrader.config.InstallationConfig;
import com.s4m.upgrader.model.Installation;

public class OSHelper {
	
	private final String platformDependencyString;
	private final OSPlatform platform;
	
	private static final Logger LOGGER = Logger.getLogger(OSHelper.class);
	private static final String OS_NAME = System.getProperty("os.name").toLowerCase();
	private static final OSHelper INSTANCE = new OSHelper();
		
	public String getPlatformDependencyString() {
		return platformDependencyString;
	}

	public static OSHelper getInstance() {
		return INSTANCE;
	}
	
	private OSHelper() {
		if(OS_NAME.indexOf("win") >= 0) {
			this.platform = OSPlatform.WINDOWS;
			this.platformDependencyString = "win32.x86_64";
		} else if(OS_NAME.indexOf("mac") >= 0) {
			this.platform = OSPlatform.MAC;
			//this.platformDependencyString = "macos";
			this.platformDependencyString = "linux"; // HACK !!!!!!!!!!!!!!!!!
		} else if(OS_NAME.indexOf("sunos") >= 0) {
			this.platform = OSPlatform.SOLARIS;
			this.platformDependencyString = "solaris";
		} else if (OS_NAME.indexOf("nix") >= 0 || OS_NAME.indexOf("nux") >= 0 || OS_NAME.indexOf("aix") > 0 ) {
			this.platform = OSPlatform.UNIX;
			this.platformDependencyString = "linux";
		} else {
			this.platform = OSPlatform.OTHER;
			this.platformDependencyString = "other";
		}

		LOGGER.info("OS platform: " + this.platform );	
		
	}
	
	public enum OSPlatform {
		WINDOWS, UNIX, MAC, SOLARIS, OTHER
	}
	
	public void isSupportedPlatform() {
		if(this.platform != OSPlatform.UNIX && this.platform != OSPlatform.WINDOWS && this.platform != OSPlatform.MAC) {
    		String unsupportedPlatformLogMessage = "OS platform " + this.platform + " is not supported!";
    		System.out.println("" + unsupportedPlatformLogMessage + "\n");
    		LOGGER.error(unsupportedPlatformLogMessage);
    		throw new IllegalArgumentException(unsupportedPlatformLogMessage);
    	}
		System.out.println("OS platform: " + this.platform);
	}
	
	public void mongoDump( InstallationConfig installationCfg, Installation installation, Path backupPath ) {
		String[] env = {};

		for (String mongoDbName : installation.getApplication().getMongoDbNames()) {
			
			if (installationCfg.getMongoPrefix() != null) {
				if (installationCfg.getMongoPrefix().length() > 0 ) {
					mongoDbName = installationCfg.getMongoPrefix() + "_" + mongoDbName;
				}
			}
			
			String msgMongoDumpErr = "Failed to perform MongoDump on DB '" + mongoDbName + "'!\nPerform the operation manually.";
			
			if (this.platform == OSPlatform.WINDOWS) {
				String[] script = { App.appConfig.getMongoServerBinPath() + "\\mongodump.exe", "--gzip", "--db", mongoDbName, "--out", Paths.get(backupPath.toString(), "mongo").toString() };
				try {
					Runtime.getRuntime().exec(script, env, new File(App.appConfig.getMongoServerBinPath()));
					this.mongoDumpInfo(mongoDbName);
				} catch (Exception e) {
					LOGGER.error(msgMongoDumpErr, e);
					System.out.println(msgMongoDumpErr + ": " + e);
					ConsoleHelper.getInstance().pressEnterToContinue();
				}
				
			} else if (this.platform == OSPlatform.UNIX || this.platform == OSPlatform.MAC) {
				String[] script = { "mongodump", "--gzip", "--db", mongoDbName, "--out", Paths.get(backupPath.toString(), "mongo").toString() };
				try {
					Runtime.getRuntime().exec(script);
					this.mongoDumpInfo(mongoDbName);
				} catch (Exception e) {
					LOGGER.error(msgMongoDumpErr, e);
					System.out.println(msgMongoDumpErr + ": " + e);
					ConsoleHelper.getInstance().pressEnterToContinue();
				}				
				
			} else {
				String msgMongoDumpNotImpl = "Operation MongoDump is not implemented for platfrom " + this.platform + "\nPerform the operation manually.";
				LOGGER.error(msgMongoDumpNotImpl);
				System.out.println(msgMongoDumpNotImpl);
				ConsoleHelper.getInstance().pressEnterToContinue();			
			}
			
		} 
		
	}
	
	public void serviceDirector(InstallationConfig installationCfg, Installation installation, String command) {
		String msgServiceOperationFault = "Failed to perform " + command + " service " + installationCfg.getServiceName() + "!\nPerform the operation manually.";

		if (this.platform == OSPlatform.WINDOWS) {
			String[] script = {"cmd.exe", "/c", "sc", command, installationCfg.getServiceName()};
			try {
				Runtime.getRuntime().exec(script);
				
				this.serviceDirectorInfo(installationCfg, command);
				
			} catch (IOException e) {
				LOGGER.error(msgServiceOperationFault, e);
				System.out.println(msgServiceOperationFault + ": " + e);
				ConsoleHelper.getInstance().pressEnterToContinue();
			}
			
		} else if (this.platform == OSPlatform.UNIX || this.platform == OSPlatform.MAC) {

			try {
				String[] script = { "./"+ installation.getApplication().getShellCommand(), command };
				String[] env = {};
				
				Runtime.getRuntime().exec(script, env, Paths.get(installation.getAppLocation()).toFile() );
				
				this.serviceDirectorInfo(installationCfg, command);
				
			} catch (IOException e) {
				LOGGER.error(msgServiceOperationFault, e);
				System.out.println(msgServiceOperationFault + ": " + e);
				ConsoleHelper.getInstance().pressEnterToContinue();
			}
			
		} else {
			String msgServiceOperationNotImpl = "Operation " + command + " is not implemented for platform " + this.platform + "!\nPerform the operation manually.";
			LOGGER.error(msgServiceOperationNotImpl);
			System.out.println(msgServiceOperationNotImpl);
			ConsoleHelper.getInstance().pressEnterToContinue();
		}
					
	}
	
	private void mongoDumpInfo(String mongoDbName) {
		String msgMongoDumpInfo = "MongoDump on DB " + mongoDbName + " is finished.";
		System.out.println(msgMongoDumpInfo);
		LOGGER.info(msgMongoDumpInfo);
	}
	
	private void serviceDirectorInfo(InstallationConfig installationCfg, String command) {
		String msgService = "Service: " + installationCfg.getServiceName() + ", command: " + command;
		System.out.println(msgService);
		LOGGER.info(msgService);
		System.out.println("Please check if the service status is correct.");
		ConsoleHelper.getInstance().pressEnterToContinue();
	}
	
}

