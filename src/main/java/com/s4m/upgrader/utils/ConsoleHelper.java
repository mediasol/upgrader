package com.s4m.upgrader.utils;

import java.util.Scanner;
import org.apache.commons.io.input.CloseShieldInputStream;
//import org.apache.log4j.Logger;

public class ConsoleHelper {
	
	//private final static Logger LOGGER = Logger.getLogger(ConsoleHelper.class);

	private static final ConsoleHelper INSTANCE = new ConsoleHelper();
	
	public static ConsoleHelper getInstance() {
		return INSTANCE;
	}
	
	public boolean askYesNo(String message) {
    	
		int input = -1;
		do {
			input = this.readerYesNo(message);	
		} while (input < 0); 
		
		if(input > 0) { return true; } else { return false; }
		
	} 
	
	public void pressEnterToContinue() {
		System.out.println("Press Enter to continue");
		try{ System.in.read(); }
		catch (Exception e) { }
	}
	
	private int readerYesNo(String message) {
		
		try (Scanner reader = new Scanner(new CloseShieldInputStream(System.in))) {
    		System.out.print(message + "\n[Y]es or [N]o: ");
    		String input = reader.next();

			if (input.toLowerCase().trim().equals("y")) { 
				return 1; 
			} else if (input.toLowerCase().trim().equals("n")) {
				return 0;
			} else {
				return -1;
			}
													
    	}
	}
	
}