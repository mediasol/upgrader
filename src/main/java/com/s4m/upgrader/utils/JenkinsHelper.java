package com.s4m.upgrader.utils;
 
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.Artifact;
import com.offbytwo.jenkins.model.BuildWithDetails;
import com.offbytwo.jenkins.model.JobWithDetails;

import com.s4m.upgrader.App;
import com.s4m.upgrader.config.InstallationConfig;

// https://www.programcreek.com/java-api-examples/index.php?api=com.offbytwo.jenkins.JenkinsServer

public class JenkinsHelper {
	
	private final static Logger logger = Logger.getLogger(JenkinsHelper.class);
	
	private JenkinsServer builder;
	private String pipeline;
	private InstallationConfig installationConfing;
	private BuildWithDetails lastSuccesBuild;
	private Artifact productArtifact;
	private ArrayList<Artifact> pluginArtifacts = new ArrayList<Artifact>();
	private List<Artifact> artifacts;
	
	public Artifact getProductArtifact() {
		return productArtifact;
	}

	public ArrayList<Artifact> getPluginArtifacts() {
		return pluginArtifacts;
	}

	public JenkinsHelper(String userName, String password, String pipeline, InstallationConfig installationConfing) {
		this.pipeline = pipeline;
		this.installationConfing = installationConfing;
		this.login(userName, password);
	}
	
	public void run() {
		
		if(!this.builder.isRunning()) {
			String msgLoginToBuilderError = "Failed to sign up for a builder '" + App.appConfig.getBuilderUrl() + "', or builder is OFF!!";
			logger.error(msgLoginToBuilderError);
			System.out.println(msgLoginToBuilderError + "\n");
			throw new RuntimeException(msgLoginToBuilderError);			
		}
		
		this.init();
						
	}
		
	public void downloadFiles(Path targetPath) {
		Path newTargetPath = Paths.get(targetPath.toString());
		
		this.prepareTargetPath(newTargetPath);
		this.downloadFile(newTargetPath, this.productArtifact);
		this.pluginArtifacts.forEach(artifact -> this.downloadFile(newTargetPath, artifact));
		
	}
	
	private void downloadFile(Path targetPath, Artifact artifact) {
		
		try {
			targetPath = Paths.get(targetPath.toString(), artifact.getFileName());
			
			InputStream is = this.lastSuccesBuild.downloadArtifact(artifact);
			OutputStream os = new FileOutputStream(targetPath.toFile());
			
			System.out.print("Downloading " + artifact.getFileName() + " ...");
			logger.info("Downloading " + artifact.getFileName());
			
			int read = 0;
			byte[] bytes = new byte[1024];
			
			while ((read = is.read(bytes)) != -1) {
				os.write(bytes, 0, read);
			}

			is.close();
			os.close();
			System.out.println(" Done!");
			
		} catch (IOException e) {

			logger.error(e.getMessage());
			e.printStackTrace();
		} catch (URISyntaxException e) {
			
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	private void prepareTargetPath(Path targetPath) {
		if(Files.exists(targetPath)) {
			try {
				String msgClean = "Cleaning directory '" + targetPath.toString() + "'";
				System.out.println(msgClean);
				logger.info(msgClean);
				FileUtils.cleanDirectory(targetPath.toFile());
			} catch (IOException e) {
				System.out.println(e.getMessage());
				logger.error(e.getMessage());
				e.printStackTrace();
			}
		}
		
		if (!Files.exists(targetPath)) {
			//System.out.println("Path '" + targetPath + "' does not exist!");
			//if (ConsoleHelper.getInstance().askYesNo("Do you want to create this path?")) {
				File f = new File(targetPath.toString());
				if (f.mkdirs()) {
	                String msgMkDir = "Path '" + targetPath + "' successfully created.";
					System.out.println(msgMkDir);	
					logger.info(msgMkDir);
				}
			//}
		}
	}
	
	
	private void init() {
		this.lastSuccesBuild = this.getLastSuccessBuild();

		if(lastSuccesBuild.isBuilding()) {
			String msgBuilderIsBuilding = "This build (" + lastSuccesBuild.getNumber() + ") is in progres! Try again later.";
			logger.error(msgBuilderIsBuilding);
			System.out.println(msgBuilderIsBuilding + "\n");
			throw new RuntimeException(msgBuilderIsBuilding);				
		}
		
		artifacts = lastSuccesBuild.getArtifacts();
		this.getProductArtifact(OSHelper.getInstance().getPlatformDependencyString());
		this.getPlugins();
		
		String msgBuilderInfo = "Pipeline: " + this.pipeline + ", last success build no.: " + lastSuccesBuild.getNumber();
		System.out.println(msgBuilderInfo);
		logger.info(msgBuilderInfo);
		
		msgBuilderInfo = "Product artifact filename: " + this.productArtifact.getFileName();
		System.out.println(msgBuilderInfo);
		this.pluginArtifacts.forEach(plugin -> System.out.println("Plugin artifact filename: " + plugin.getFileName()));
		System.out.println();
	}
	
	private void getPlugins() {
		this.installationConfing.getPlugins().forEach(this::getPluginArtifact);
	}
	
	private void getPluginArtifact(String plugin) {
		if (plugin == null || plugin.isEmpty()) {
			return;
		}
		this.pluginArtifacts.add(this.getArtifactByString(plugin));
	}
	
	private void getProductArtifact(String platformDependencyString) {
		if (platformDependencyString == null || platformDependencyString.isEmpty()) throw new IllegalArgumentException("Error while input platformDependencyString cannot be null or empty!!");
		this.productArtifact = this.getArtifactByString(platformDependencyString);
	}
	
	private Artifact getArtifactByString(String text) {
		return this.artifacts.stream().filter(artifact -> artifact.getFileName().contains(text)).findFirst().get();
	}
	
	private BuildWithDetails getLastSuccessBuild() {
		try {
			JobWithDetails detailedJob = this.builder.getJob(this.pipeline);
		    return detailedJob.getLastSuccessfulBuild().details();
		} catch (IOException e) {
	    	
	    } 
		return null;
	}
		
	private void login(String userName, String password) {
	   
	   try {
		   URI uri = new URI(App.appConfig.getBuilderUrl());
		   this.builder = new JenkinsServer(uri, userName, password);
		   
		   String msgLoggedToBuilder = "Signed in to a builder '" + App.appConfig.getBuilderUrl() + "'";
		   logger.info(msgLoggedToBuilder);
		   System.out.println(msgLoggedToBuilder);
		  
	   } catch (URISyntaxException e) {
		   String msgLoginToBuilderError = "Failed to sign up for a builder '" + App.appConfig.getBuilderUrl() + "'!!";
		   logger.error(msgLoginToBuilderError);
		   System.out.println(msgLoginToBuilderError + "\n");
		   throw new RuntimeException(msgLoginToBuilderError, e);
	   }
	   
   }
}