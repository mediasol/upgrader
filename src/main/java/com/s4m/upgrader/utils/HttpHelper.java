package com.s4m.upgrader.utils;

//import org.joda.time.DateTime;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.function.Function;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

public class HttpHelper {
	
	private final static Logger log = Logger.getLogger(HttpHelper.class);
	private static final String apiToken = "ba22a6e61f18f9b4e8c448c87ea54224";
	
	public <T> T sendPost(String url, String jsonContent, String contentType, HashMap<String, String> headerParams, Function<CloseableHttpResponse, T> processor) {
		HttpClientBuilder b = HttpClientBuilder.create();
		try (CloseableHttpClient client = b.build()) {
			HttpPost post = new HttpPost(url);
			
			headerParams.forEach((k, v) -> post.addHeader(k, v));
			
			StringEntity entity = new StringEntity(jsonContent, ContentType.create(contentType, StandardCharsets.UTF_8));
			post.setEntity(entity);
			try (CloseableHttpResponse resp = executePost(client, url, post)) {
				return processor.apply(resp);
			}
		} catch (IOException e) {
			log.error("Error while executing ticket test method to url!!", e);
			throw new RuntimeException(e);
		}
	}
	
	public <T> T sendGet(String url, boolean authTokenHeader, Function<CloseableHttpResponse, T> processor) {
		HttpClientBuilder b = HttpClientBuilder.create();
		try (CloseableHttpClient client = b.build()) {
			HttpGet get = new HttpGet(url);
			
			//get.addHeader("Accept", "application/json");
			get.addHeader("Content-Type", "application/x-www-form-urlencoded");
			if (authTokenHeader) {
				get.addHeader("Token", getToken());
			}
			
			try (CloseableHttpResponse resp = executeGet(client, url, get)) {
				log.info(resp);
				
				return processor.apply(resp);
			}
		} catch (IOException e) {
			log.error("Error while executing ticket test method to url!!", e);
			throw new RuntimeException(e);
		}
	}
	
	
	private static CloseableHttpResponse executePost(CloseableHttpClient client, String url, HttpPost post) {
		try {
			return client.execute(post);
		} catch (Exception e) {
			log.info("Error throw execution of request to address '" + url + "'!!! Maybe server is OFF??", e);
			throw new IllegalStateException("Error whew execution request to address '" + url + "'!!! Maybe server is OFF??", e);
		}
	}
	
	
	private static CloseableHttpResponse executeGet(CloseableHttpClient client, String url, HttpGet get) {
		try {
			return client.execute(get);
		} catch (Exception e) {
			log.info("Error throw execution of request to address '" + url + "'!!! Maybe server is OFF??", e);
			throw new IllegalStateException("Error whew execution request to address '" + url + "'!!! Maybe server is OFF??", e);
		}
	}
	
	public static String getToken() {
		LocalDateTime nowDate = LocalDateTime.now(ZoneOffset.UTC);
		nowDate = nowDate.withMinute(0).withSecond(0).withNano(0);
		long localDTInMilli = nowDate.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
		
        try {
        	String token = Long.toString(localDTInMilli) + apiToken + Long.toString(localDTInMilli);
        	MessageDigest md = MessageDigest.getInstance("MD5");
        	
            md.update(token.getBytes());
            
            byte byteData[] = md.digest();
     
            //convert the byte to hex format
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
            	sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            
            return sb.toString();
    		
        } catch (NoSuchAlgorithmException e) {
        	log.error(e.getStackTrace());
        } 
        
        return null;
       
	}
}



