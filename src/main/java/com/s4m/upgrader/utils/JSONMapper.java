package com.s4m.upgrader.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.io.File;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONMapper {
	
	// convert map to JSON string
	public static String toJSON(Map<String, Object> map) {
		ObjectMapper mapper = new ObjectMapper();
		String json = "";
		try {
			//json = mapper.writeValueAsString(map);
			json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map);
			//System.out.println(json);

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return json;
	}
	
	// convert JSON string to Map
	public static Map<String, Object> toMap(String json) {
		Map<String, Object> map = new HashMap<String, Object>();
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			map = mapper.readValue(json, new TypeReference<Map<String, String>>(){});
			//System.out.println(map);
			
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	// write map to JSON file
	public static void toJSONFile(Map<String, Object> map, String name) {
		try {

			ObjectMapper mapper = new ObjectMapper();
			String workingDir = System.getProperty("user.dir");
			mapper.writeValue(new File(workingDir + "/" + name + ".json"), map);

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// load JSON file to map
	public static Map<String, Object> loadJSONFile(String name) {
		try {

			ObjectMapper mapper = new ObjectMapper();
			String workingDir = System.getProperty("user.dir");
			Map<String, Object> map = mapper.readValue(
					new File(workingDir + "/" + ".json"), 
					new TypeReference<Map<String, Object>>() {});
			return map;
			
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}