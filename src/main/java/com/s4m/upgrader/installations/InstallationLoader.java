package com.s4m.upgrader.installations;

import java.util.List;

import com.s4m.upgrader.model.Installation;

public interface InstallationLoader {

	List<Installation> getInstallations();
	
	String getApiUserCredentialsHash();

	Installation getInstallation(String id);

}
