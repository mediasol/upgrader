package com.s4m.upgrader.installations;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.s4m.upgrader.App;
import com.s4m.upgrader.model.Installation;
import com.s4m.upgrader.utils.HttpHelper;
import com.s4m.upgrader.utils.JSONMapper;

public class InstallationsLoaderImpl implements InstallationLoader {

	private final static Logger logger = Logger.getLogger(InstallationsLoaderImpl.class);
	
	@Override
	public List<Installation> getInstallations() {
		return new HttpHelper().sendGet(App.appConfig.getCmlUrl() + "/installations", true, resp -> parseInstallations(resp));
	}
	
	public String getApiUserCredentialsHash() {
		return new HttpHelper().sendGet(App.appConfig.getCmlUrl() + "/builds", true, resp -> parseApiUserCredentialsHash(resp));

	}
	
	private String parseApiUserCredentialsHash(CloseableHttpResponse resp) { 
		try (BufferedReader r = new BufferedReader(new InputStreamReader(resp.getEntity().getContent(), StandardCharsets.UTF_8))) { 
			String line = null;
			String message = new String();
			while ((line = r.readLine()) != null) {
				message += line;
			}
			
			return JSONMapper.toMap(message).get("hash").toString();
		} catch (Exception e) {
			logger.error("Error while loading data about API user!", e);
			throw new IllegalStateException(e);
		}
	}
	
	private List<Installation> parseInstallations(CloseableHttpResponse resp) {
		try (BufferedReader r = new BufferedReader(new InputStreamReader(resp.getEntity().getContent(), StandardCharsets.UTF_8))) {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			return mapper.readValue(r, new TypeReference<List<Installation>>(){});
		} catch (Exception e) {
			logger.error("Error while loading data about installations!", e);
			throw new IllegalStateException(e);
		}
	}

	@Override
	public Installation getInstallation(String instanceID) {
		if (instanceID == null || instanceID.isEmpty()) throw new IllegalArgumentException("Error while input id cannot be null or empty!!");
		return getInstallations().stream().filter(i -> instanceID.equals(i.getId())).findFirst().get();
	}
	
}
