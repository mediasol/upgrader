package com.s4m.upgrader.upgrades;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.ini4j.Wini;
import com.offbytwo.jenkins.model.Artifact;
import com.s4m.upgrader.App;
import com.s4m.upgrader.config.InstallationConfig;
import com.s4m.upgrader.installations.InstallationsLoaderImpl;
import com.s4m.upgrader.model.Installation;
import com.s4m.upgrader.utils.ConsoleHelper;
import com.s4m.upgrader.utils.HttpHelper;
import com.s4m.upgrader.utils.JenkinsHelper;
import com.s4m.upgrader.utils.OSHelper;

public class Upgrader {

	final static Logger logger = Logger.getLogger(Upgrader.class);
	private final Installation installation;
	private final InstallationConfig installationConfig;
	private String selectedBranch;
	
	private String userName = "bitbucket64";
	private String password = "1a927d2814a900d2c89c408d";
	

	public Upgrader(Installation installation, InstallationConfig installationConfig, String selectedBranch) {
		this.installation = installation;
		this.installationConfig = installationConfig;
		this.selectedBranch = selectedBranch;

	}

	public void run() {
		
		String pipeline = installation.getApplication().getApplicationId() + "-" + this.selectedBranch;
		
		String apiUserInfo = new InstallationsLoaderImpl().getApiUserCredentialsHash();
    	String[] credentials = this.getCredentialsFromHash(apiUserInfo);
    	userName = credentials[0];
    	password = credentials[1];

		// get info from builder
    	JenkinsHelper builder = new JenkinsHelper(userName, password, pipeline, this.installationConfig);
		builder.run();
				
		// stop service
		OSHelper.getInstance().serviceDirector(this.installationConfig, this.installation, "stop");

		// backup
		Path backupPath = this.backUp();
		
		// backup MongoDb
		this.mongoBackUp(backupPath);
		
		// download
		this.download(builder);
		
		//upgrade
		this.upgrade(builder);
		
		// start service
		OSHelper.getInstance().serviceDirector(this.installationConfig, this.installation, "start");
	
		System.out.println("Upgrade finished.");
	}
	
	public void handJob() {
		System.out.println("\nNow perform the actions on the basis of the upgrade document (if any).");
		ConsoleHelper.getInstance().pressEnterToContinue();		
	}
	
	public void upgrade(JenkinsHelper builder) {
		if(!ConsoleHelper.getInstance().askYesNo("\nDo you want to upgrade " + this.installation.getApplication().getName() + " application?")) {
			return;
		}
		
		// delete application
		this.deleteApplication();
		
		// unzip new application data
		this.unzipApplication(builder);
		
		// copy plugins
		this.copyPlugins(builder);

		// delete data/configs/upgradeUserStatus.xmi (only CFM and NX)
		this.setUpgradeUserStatus(this.installation.getWsLocation());
		
		// hand job by Upgrade Document (tm)
		this.handJob();
		
	}
	
	public void copyPlugins(JenkinsHelper builder) {
		if (!builder.getPluginArtifacts().isEmpty()) {
			
			if(!ConsoleHelper.getInstance().askYesNo("\nDo you want to copy plugins to new version of application now?")) {
				return;
			}
			
			System.out.println("Copying new version of plugins to application ...");
			builder.getPluginArtifacts().forEach(artifact -> copyPlugin(artifact, Paths.get(this.installation.getAppLocation(), "plugins").toFile()));
		
			// write plugins to configuration/config.ini
			this.writePlugins( Paths.get(this.installation.getAppLocation(), "configuration", "config.ini").toFile(), builder.getPluginArtifacts() );

		}
	}
	
	public void unzipApplication(JenkinsHelper builder) {
		if(!ConsoleHelper.getInstance().askYesNo("\nDo you want to unzip new version of application now?")) {
			return;
		}
		
		System.out.println("Unzipping new version of application ...");
		try {
			unzip(Paths.get(App.appConfig.getUpgradePath(), builder.getProductArtifact().getFileName()), installation.getAppLocation(), installation.getUntouchableFiles());
		} catch (IOException e) {
			String msgUnzipError = "An error occurred while unzip " + builder.getProductArtifact().getFileName();
			System.out.println(msgUnzipError);
			logger.error(msgUnzipError, e);
			throw new IllegalArgumentException(msgUnzipError, e);
		}		
		
	}
	
	public void deleteApplication() {
		if(!ConsoleHelper.getInstance().askYesNo("\nDo you want to delete current application now?")) {
			return;
		}
		
		// deleting old application data
		System.out.println("Deleting application ...");
		deleteDir(Paths.get(this.installation.getAppLocation()), installation.getUntouchableFiles());
	}
	
	public void download(JenkinsHelper builder) {
		if(ConsoleHelper.getInstance().askYesNo("\nDo you want to download files now?")) {
			builder.downloadFiles(Paths.get(App.appConfig.getUpgradePath()));
		}
	}
	
	private void setUpgradeUserStatus(String wsPath) {
		if (this.installation.getApplication().getApplicationId() == "CFMBridge") {
			return;
		}
		
		String msgDeleteUpgradeStatus = "\nUser upgrade status setting ...";
		System.out.println(msgDeleteUpgradeStatus);
		logger.info(msgDeleteUpgradeStatus);
		
		File upgradeStatusFile = Paths.get(wsPath, "configs", "upgradeUserStatus.xmi").toFile();
		upgradeStatusFile.delete();
	}
	
	private void writePlugins(File configIni, List<Artifact> plugins) {
		try {
			
			Wini ini = new Wini(configIni);
			
			String bundlesReference = ini.get("?", "osgi.bundles");
			ArrayList<String> references = new ArrayList<String>();
			references.addAll(Arrays.asList(bundlesReference.split(",")));
			
			for (Artifact plugin : plugins ) {
				references.add("reference\\:file\\:" + plugin.getFileName() + "@4");
			}
			ini.put("?", "osgi.bundles", String.join(",", references));
			ini.store();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void copyPlugin(Artifact artifact, File targetPluginsPath) {
		try {
			System.out.print(artifact.getFileName() + " ... ");
			FileUtils.copyFile(Paths.get(App.appConfig.getUpgradePath(), artifact.getFileName()).toFile(), Paths.get(targetPluginsPath.toString(), artifact.getFileName()).toFile());
			System.out.println("OK");
		} catch (IOException e) {
			System.out.println("ERROR");
			String msgCopyError = "An error occurred while copy " + Paths.get(App.appConfig.getUpgradePath(), artifact.getFileName()) + " to " + targetPluginsPath;
			System.out.println(msgCopyError);
			logger.error(msgCopyError);
			e.printStackTrace();
			throw new IllegalArgumentException(msgCopyError);
		}
	}
	
	private void unzip(Path fileZip, String targetPath, List<Path> excludedFiles) throws IOException {
        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(fileZip.toFile()))) {
        	ZipEntry zipEntry;
            while ((zipEntry = zis.getNextEntry()) != null) {
                Path p = Paths.get(targetPath, zipEntry.getName());
                
                if (isUsable(p, excludedFiles)) { 
                	extract(zis, zipEntry, p); 
                }
                zis.closeEntry();
            }
        }
    }

	private void extract(ZipInputStream zis, ZipEntry zipEntry, Path p) throws IOException {
		if (zipEntry.isDirectory()) {
			Files.createDirectories(p);
			return;
		}
		Files.copy(zis, p);
	}
	
	private void deleteDir(Path sourceDirectory, List<Path> excludedFiles) {
		Stream<Path> files = getFiles(sourceDirectory);
		files.filter(p -> isUsable(p, excludedFiles)).forEach(file -> delete(file, excludedFiles));
	}

	private Stream<Path> getFiles(Path dir) {
		try {
			return Files.list(dir);
		} catch (IOException e) {
			throw new IllegalStateException("Error while listing dir '" + dir + "'!!", e);
		}
	}

	private void delete(Path file, List<Path> excludedFiles) {
		if (Files.isDirectory(file)) {
			deleteDir(file, excludedFiles);
			return;
		}
		delete(file);
	}

	private void delete(Path file) {
		try {
			Files.delete(file);
		} catch (IOException e) {
			throw new IllegalStateException("Error while deleting file '" + file + "'!!", e);
		}
	}
	
	private boolean isUsable(Path p, List<Path> excludedFiles) {
    	boolean result = excludedFiles.contains(p) || Files.isSymbolicLink(p);
    	return !result;
	}
	
	private void mongoBackUp(Path backupPath) {
		if (backupPath == null) {
			return;
		}
		
		if(!ConsoleHelper.getInstance().askYesNo("\nDo you want to backup MongoDB now?")) {
			return;
		}
		
		System.out.println("\nBackUp MongoDB ...");
		Path mongoPath = Paths.get(backupPath.toString(), "mongo");
		this.preparePath(mongoPath);
		
		OSHelper.getInstance().mongoDump(this.installationConfig, this.installation, backupPath);
		
	}
	
	private Path backUp() {
		if(!ConsoleHelper.getInstance().askYesNo("\nDo you want to backup files now?")) {
			return null;
		}
		
		System.out.println("BackUp application and workspace ...");
		
		Path backupPath = Paths.get(App.appConfig.getBackupPath(), this.installation.getId(), new SimpleDateFormat("yyyyMMdd_HHmm").format(new Date()));
		this.preparePath(Paths.get(backupPath.toString(), "app"));
		this.preparePath(Paths.get(backupPath.toString(), "data"));
		
		try {
			
			FileUtils.copyDirectory(new File(this.installation.getAppLocation()), Paths.get(backupPath.toString(), "app").toFile());
			String msgCopied = "Application directory '" + this.installation.getAppLocation() + "' was copied to '" + backupPath + "'";
			System.out.println(msgCopied);
			logger.info(msgCopied);
			
			FileUtils.copyDirectory(new File(this.installation.getWsLocation()), Paths.get(backupPath.toString(), "data").toFile());
			msgCopied = "Workspace directory '" + this.installation.getWsLocation() + "' was copied to '" + backupPath + "'";
			System.out.println(msgCopied);
			logger.info(msgCopied);
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
			logger.error(e.getMessage());
			e.printStackTrace();
			throw new IllegalArgumentException();
		}
		
		return backupPath;
	}
	
	private void preparePath(Path path) {
		if (!Files.exists(path)) {
			File f = new File(path.toString());
			if (f.mkdirs()) {
                String msgMkDir = "Path '" + path + "' successfully created.";
				System.out.println(msgMkDir);	
				logger.info(msgMkDir);
			}
		}
	}
	
	private String[] getCredentialsFromHash(String hash) {
		
		try {
		    byte[] base64decodedBytes = Base64.getDecoder().decode(hash);
			String encoded = new String(base64decodedBytes, "utf-8");
			
			encoded = encoded.substring(9);
			encoded = encoded.substring(0, encoded.length() - 9);
			return encoded.split(HttpHelper.getToken());
		
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			throw new IllegalArgumentException();
		}
		
	}
}
